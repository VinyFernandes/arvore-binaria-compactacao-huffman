/*
 * File:   Arvore.h
 *
 * Created on 23 de Abril de 2018, 17:48
 */

#ifndef ARVORE_H
#define ARVORE_H

#include "No.h"

class Arvore
{
public:
    Arvore();
    Arvore(int chave);
    Arvore(const Arvore& orig);
    virtual ~Arvore();
    void SetRaiz(No* raiz);
    No* GetRaiz() const;
    bool Vazio();
    int Busca(int chave, No*& raiz);
    bool Insere(int chave, No* raiz);
    No* Remove(int chave, No*& raiz);
    No* LocalizaSucessor(No* raiz);
    void PreOrdem(No* raiz);
    void EmOrdem(No* raiz);
    void PosOrdem(No* raiz);
    void EmNivel(No* raiz);
    void Visita(No* raiz);

protected:

private:
    No* raiz;
};

#endif /* ARVORE_H */

